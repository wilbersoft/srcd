package pe.gob.bn.srcd.presentacion.formulario;

public class SubirAnexo {
	private String codCtaContable;
	private String fechaCorte;
	public String getCodCtaContable() {
		return codCtaContable;
	}
	public void setCodCtaContable(String codCtaContable) {
		this.codCtaContable = codCtaContable;
	}
	public String getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
}
