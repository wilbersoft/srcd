package pe.gob.bn.srcd.presentacion.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import pe.gob.bn.srcd.controller.otros.Course;
import pe.gob.bn.srcd.dao.domain.entity.Anexo;
import pe.gob.bn.srcd.dao.domain.entity.Plantilla;
import pe.gob.bn.srcd.dao.domain.mapper.SrcdMapper;
import pe.gob.bn.srcd.presentacion.formulario.DescargarAnexo;
@Controller
@SessionAttributes
public class DescargarAnexoController {
	
	
	@Autowired
	public  SrcdMapper  srcdMapper;	
	
 
	 
	
	@RequestMapping(value = "/addDescargarAnexo", method = RequestMethod.POST)
	public ModelAndView addDescargarAnexo(@ModelAttribute("descargarAnexo")	DescargarAnexo descargarAnexo, BindingResult result) {	
		/*
		Integer valorID=srcdMapper.getIdPlantilla();		
		valorID=valorID==null?0:valorID;
		
		valorID=valorID+1;
		
		Integer rpta=srcdMapper.insertPlantilla(new Plantilla(valorID+"", "50", "test", "1", "", "admin"));
		
		
		System.out.println("rpta: "+rpta);*/
		
		List<Anexo> listAppAnexo =srcdMapper.selectAnexo(new Anexo());
		
	       // create some sample data 

/*        List<Course> listCourses = new ArrayList();
        listCourses.add(new Course(1, "Polarfrosch100", new Date()));
        listCourses.add(new Course(2, "Polarfrosch101", new Date()));
        listCourses.add(new Course(3, "Polarfrosch102", new Date()));*/

        // return a view which will be resolved by an excel view resolver
        return new ModelAndView("excelView2", "anexos", listAppAnexo);
  		
		
	}
	 
	
	@RequestMapping("/descargarAnexo")
	public String showDescargarAnexos(Model m) {
		

		Anexo anexo=new Anexo();
		List<Anexo> lsCtaContable =srcdMapper.selectAnexo(anexo);		
		m.addAttribute("lsCtaContable",lsCtaContable);
		
		return "descargarAnexo";
	}	 
	
 
	
	
}
