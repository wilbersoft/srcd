package pe.gob.bn.srcd.presentacion.controller;
import java.io.File;
 
 
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import pe.gob.bn.srcd.controller.otros.Contact;
import pe.gob.bn.srcd.dao.domain.entity.Anexo;
import pe.gob.bn.srcd.dao.domain.mapper.SrcdMapper;
import pe.gob.bn.srcd.presentacion.formulario.SubirAnexo;
import static org.apache.poi.ss.usermodel.Cell.*;

@Controller
@SessionAttributes
public class SubirAnexoController {
	
	
	@Autowired
	public  SrcdMapper  srcdMapper;	
	
	
	@RequestMapping(value = "/addSubirAnexo", method = RequestMethod.POST)
	public String addSubirAnexo(@ModelAttribute("subirAnexo")	SubirAnexo subirAnexo, BindingResult result) {	
		//write the code here to add subirAnexo
		return "redirect:subirAnexo.html";
	}
	  
	
	@RequestMapping("/subirAnexo")
	public String showSubirAnexos(Model m) {
		Anexo anexo=new Anexo();
		List<Anexo> lsCtaContable =srcdMapper.selectAnexo(anexo);		
		m.addAttribute("lsCtaContable",lsCtaContable);		
		return "subirAnexo";
	}	 
	     
	@RequestMapping("/readFileXls")
	public String readFileXls(@RequestParam CommonsMultipartFile[] fileUpload,ModelMap model) throws Exception {
		
		FileInputStream file;
		List<Anexo> lsAnexo=null;
		Anexo anexo=null;

		 
		System.out.println("-----------------"+anexo);		
		
		if (fileUpload != null && fileUpload.length > 0) {
			
			for (CommonsMultipartFile aFile : fileUpload){
				 
				// ********************** Ini ********************** //
				String fileSistOperativo = "/home/srcd/data_load/";// ---- Directorio DESARROLLO
				//String fileSistOperativo = "//reportes";// ---- Directorio QA
				File localFile = new File(fileSistOperativo);
				try {
					localFile.mkdirs();
					localFile = new File(localFile+"//"+aFile.getOriginalFilename());
			    	FileOutputStream os = null;
		    		os = new FileOutputStream(localFile);
		    		os.write(aFile.getBytes());
				} catch (Exception e) {
					e.printStackTrace();
					model.addAttribute("msjRptaError","Se presentaron problemas al guardar el Archivo XLS");
					return "redirect:subirAnexo.html";
				}
				// ********************** Fin ********************** //
				file = new FileInputStream(new File(fileSistOperativo+"//"+aFile.getOriginalFilename().toString()));				
				XSSFWorkbook workbook = new XSSFWorkbook(file);
				String fechaCorte="";
				String cuentaContable="";
				String codCliente="";	
				String f02CValor1="";
				String f02CValor2="";
				String f02CValor3="";
				String f02CValor4="";
				String f02CValor5="";
				
				String f02Importe="";
				
				try {
					XSSFSheet sheet = null;
					for(int i=0;i<workbook.getNumberOfSheets();i++){ // # hojas						
						sheet = workbook.getSheetAt(i);						
						Row row;						
						Iterator<Row> rowIterator = sheet.iterator();
						Integer numFila=0;						
						while (rowIterator.hasNext()){	// Recorremos todas las filas para mostrar el contenido de cada celda						
							row = rowIterator.next();
							Iterator<Cell> cellIterator = row.cellIterator(); // Obtenemos el iterator que permite recorres todas las celdas de una fila
							Cell celda;
							numFila++;
							
							System.out.println(" Numero de Fila "+numFila);

							// FILAS
							Integer numColumna=0;
							while (cellIterator.hasNext()){
								celda = cellIterator.next();
								numColumna = numColumna+1;
								
									if(numFila==2) {										
										    if(numColumna==1)
											fechaCorte=celda.getStringCellValue();								
											if( numColumna==2)
											cuentaContable=celda.getStringCellValue();
											if(numColumna==3)
											codCliente=celda.getStringCellValue();
											
											if(numColumna==4) {
												System.out.println(" fechaCorte:"+fechaCorte+" cuentaContable:"+cuentaContable+" codCliente:"+codCliente);
												Anexo anexoTmp=new Anexo(fechaCorte,cuentaContable,codCliente);
												lsAnexo=srcdMapper.selectAnexoById(anexoTmp);
												anexo=lsAnexo.get(0);	
											}											
									}
									if(numFila>1) {	

										if(numColumna==1) {
											fechaCorte=readCellAsString(celda); 
										}
										if(numColumna==2) {
											cuentaContable=readCellAsString(celda); 
										}
										if(numColumna==3) {
											codCliente=readCellAsString(celda); 
										}
																				
										if(numColumna==6) {
											f02Importe=readCellAsString(celda); 
										}										
										if(numColumna==7) 
											// campo 1
											f02CValor1=readCellAsString(celda);										
										if(numColumna==8) 
											// campo 2
											f02CValor2=readCellAsString(celda);														
										if(numColumna==9)  
											// campo 3
											f02CValor3=readCellAsString(celda);									
										if(numColumna==10) 
											// campo 4
											f02CValor4=readCellAsString(celda);		

										if(numColumna==11) 
											// campo 4
											f02CValor5=readCellAsString(celda);	
										
									}
									 
									System.out.println(" Columna "+numColumna+" , Valor "+readCellAsString(celda));
							 }
							if(numFila>1) {
								System.out.println("fechaCorte:"+fechaCorte+" cuentaContable:"+cuentaContable+" codCliente:"+codCliente+" f02Importe:"+f02Importe+"  f02CValor1:"+f02CValor1+" f02CValor2:"+f02CValor2+" f02CValor3:"+f02CValor3+" f02CValor4:"+f02CValor4+" f02CValor5:"+f02CValor5);
								 
								Anexo anexoBean=new Anexo(
										fechaCorte,
										cuentaContable,
										codCliente,
										new Double(f02Importe),
										f02CValor1,
										f02CValor2,
										f02CValor3,
										f02CValor4,f02CValor5);
						 
								srcdMapper.updateAnexo(anexoBean);								
							} 
						}		
					}					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}	
		return "redirect:subirAnexo.html";
	}
	
	private String readCellAsString(final Cell cell) {
	    if (cell == null) {
	        return "";
	    }

	    switch (cell.getCellType()) {
	        case CELL_TYPE_STRING:
	            return cell.getStringCellValue();
	        case CELL_TYPE_BLANK:
	            return "";
	        case CELL_TYPE_BOOLEAN:
	            return Boolean.toString(cell.getBooleanCellValue());
	        case CELL_TYPE_NUMERIC:
	            final DataFormatter formatter = new DataFormatter();
	            return formatter.formatCellValue(cell);
	        default:
	            throw new RuntimeException("unknown cell type " + cell.getCellType());
	    }

	}
	
}
