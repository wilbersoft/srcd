package pe.gob.bn.srcd.presentacion.controller;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;

import pe.gob.bn.srcd.common.Constante;
import pe.gob.bn.srcd.controller.otros.Course;
import pe.gob.bn.srcd.dao.domain.entity.Anexo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.util.List;
import java.util.Map;

public class XlsxStreamingView extends AbstractXlsxStreamingView {

    private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);
 
    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                       Workbook workbook, HttpServletRequest request,
                                       HttpServletResponse response) throws Exception {
        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\"anexo.xlsx\"");

        @SuppressWarnings("unchecked")
        List<Anexo> anexos = (List<Anexo>) model.get("anexos");

        Anexo appAnexo=anexos.get(0);
        
        // create excel xls sheet
        Sheet sheet = workbook.createSheet(appAnexo.getF02CuentaContable());
 
        // create header row
        Row header = sheet.createRow(0); 
    	
    	header.createCell(0).setCellValue(Constante.SF_GLOBAL_TABLA_PLANTILLA_LABEL_FECHA_CORTE);
    	header.createCell(1).setCellValue(Constante.SF_GLOBAL_TABLA_PLANTILLA_LABEL_CUENTA_CONTABLE);
        header.createCell(2).setCellValue(Constante.SF_GLOBAL_TABLA_PLANTILLA_LABEL_CODIGO_CLIENTE);
        header.createCell(3).setCellValue(Constante.SF_GLOBAL_TABLA_PLANTILLA_LABEL_NOMBRE_CLIENTE);
        header.createCell(4).setCellValue(Constante.SF_GLOBAL_TABLA_PLANTILLA_LABEL_MONEDA);
        header.createCell(5).setCellValue(Constante.SF_GLOBAL_TABLA_PLANTILLA_LABEL_IMPORTE);
          
        if(appAnexo.getF02CEtiqueta1()!=null)header.createCell(6).setCellValue(appAnexo.getF02CEtiqueta1());        
        if(appAnexo.getF02CEtiqueta2()!=null)header.createCell(7).setCellValue(appAnexo.getF02CEtiqueta2());
        if(appAnexo.getF02CEtiqueta3()!=null)header.createCell(8).setCellValue(appAnexo.getF02CEtiqueta3());
        if(appAnexo.getF02CEtiqueta4()!=null)header.createCell(9).setCellValue(appAnexo.getF02CEtiqueta4());
        
        if(appAnexo.getF02CEtiqueta5()!=null) header.createCell(10).setCellValue(appAnexo.getF02CEtiqueta5());        
        if(appAnexo.getF02CEtiqueta6()!=null) header.createCell(11).setCellValue(appAnexo.getF02CEtiqueta6());
        if(appAnexo.getF02CEtiqueta7()!=null) header.createCell(12).setCellValue(appAnexo.getF02CEtiqueta7());
        if(appAnexo.getF02CEtiqueta8()!=null) header.createCell(13).setCellValue(appAnexo.getF02CEtiqueta8());
        if(appAnexo.getF02CEtiqueta9()!=null) header.createCell(14).setCellValue(appAnexo.getF02CEtiqueta9());
        if(appAnexo.getF02CEtiqueta10()!=null) header.createCell(15).setCellValue(appAnexo.getF02CEtiqueta10());
        
        // Create data cells
        int rowCount = 1;
        
        for (Anexo anexo : anexos){
            Row courseRow = sheet.createRow(rowCount++);
            
           
           
            courseRow.createCell(0).setCellValue(anexo.getF02FechaCorte()+"");            
            courseRow.createCell(1).setCellValue(anexo.getF02CuentaContable()+"");
            courseRow.createCell(2).setCellValue(anexo.getF02CodigoCliente()+"");            
            courseRow.createCell(3).setCellValue(anexo.getF02NombreCliente()+"");
            courseRow.createCell(4).setCellValue(anexo.getF02Moneda()+"");
            courseRow.createCell(5).setCellValue(anexo.getF02Importe()+"");
            
            if(appAnexo.getF02CEtiqueta1()!=null){
            	switch(anexo.getF02CTipoDato1()) {            	
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(6).setCellValue(anexo.getF02CValor1());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(6).setCellValue(new String(anexo.getF02CValor1()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(6).setCellValue(new String(anexo.getF02CValor1()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(6).setCellValue(new String(anexo.getF02CValor1()));
            	    break;            	    
            	  default:
            	} 
            }

            if(appAnexo.getF02CEtiqueta2()!=null){
            	switch(anexo.getF02CTipoDato2()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(7).setCellValue(anexo.getF02CValor2());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(7).setCellValue(new String(anexo.getF02CValor2()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(7).setCellValue(new String(anexo.getF02CValor2()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(7).setCellValue(new String(anexo.getF02CValor2()));
            	    break;            	    
            	  default:
            	} 
            }            
            
             if(appAnexo.getF02CEtiqueta3()!=null){
            	switch(anexo.getF02CTipoDato3()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(8).setCellValue(anexo.getF02CValor3());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(8).setCellValue(new String(anexo.getF02CValor3()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(8).setCellValue(new String(anexo.getF02CValor3()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(8).setCellValue(new String(anexo.getF02CValor3()));
            	    break;            	    
            	  default:
            	} 
            }              
            
            if(appAnexo.getF02CEtiqueta4()!=null){
            	switch(anexo.getF02CTipoDato4()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(9).setCellValue(anexo.getF02CValor4());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(9).setCellValue(new String(anexo.getF02CValor4()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(9).setCellValue(new String(anexo.getF02CValor4()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(9).setCellValue(new String(anexo.getF02CValor4()));
            	    break;            	    
            	  default:
            	} 
            }            
           
            if(appAnexo.getF02CEtiqueta5()!=null){
            	switch(anexo.getF02CTipoDato5()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(10).setCellValue(anexo.getF02CValor5());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(10).setCellValue(new String(anexo.getF02CValor5()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(10).setCellValue(new String(anexo.getF02CValor5()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(10).setCellValue(new String(anexo.getF02CValor5()));
            	    break;            	    
            	  default:
            	} 
            } 
          
            if(appAnexo.getF02CEtiqueta6()!=null){
            	switch(anexo.getF02CTipoDato6()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(11).setCellValue(anexo.getF02CValor6());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(11).setCellValue(new String(anexo.getF02CValor6()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(11).setCellValue(new String(anexo.getF02CValor6()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(11).setCellValue(new String(anexo.getF02CValor6()));
            	    break;            	    
            	  default:
            	} 
            }

            if(appAnexo.getF02CEtiqueta7()!=null){
            	switch(anexo.getF02CTipoDato7()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(12).setCellValue(anexo.getF02CValor7());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(12).setCellValue(new String(anexo.getF02CValor7()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(12).setCellValue(new String(anexo.getF02CValor7()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(12).setCellValue(new String(anexo.getF02CValor7()));
            	    break;            	    
            	  default:
            	} 
            }
            
            if(appAnexo.getF02CEtiqueta8()!=null){
            	switch(anexo.getF02CTipoDato8()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(13).setCellValue(anexo.getF02CValor8());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(13).setCellValue(new String(anexo.getF02CValor8()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(13).setCellValue(new String(anexo.getF02CValor8()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(13).setCellValue(new String(anexo.getF02CValor8()));
            	    break;            	    
            	  default:
            	} 
            }
       
            if(appAnexo.getF02CEtiqueta9()!=null){
            	switch(anexo.getF02CTipoDato9()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(14).setCellValue(anexo.getF02CValor9());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(14).setCellValue(new String(anexo.getF02CValor9()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(14).setCellValue(new String(anexo.getF02CValor9()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(14).setCellValue(new String(anexo.getF02CValor9()));
            	    break;            	    
            	  default:
            	} 
            }
            
            if(appAnexo.getF02CEtiqueta10()!=null){
            	switch(anexo.getF02CTipoDato10()) {
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ALFANUMERICO_5:            	  
            		  courseRow.createCell(15).setCellValue(anexo.getF02CValor10());
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_NUMERICO_6:
            		  courseRow.createCell(15).setCellValue(new String(anexo.getF02CValor10()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_ENTERO_7:
            		  courseRow.createCell(15).setCellValue(new String(anexo.getF02CValor10()));
            		  break;
            	  case Constante.SF_GLOBAL_TABLA_TIPO_DE_DATO_3_PARAMETRO_SWITCH_FECHA_8:
            		  courseRow.createCell(15).setCellValue(new String(anexo.getF02CValor10()));
            	    break;            	    
            	  default:
            	} 
            }
            
            
            
             
        }
    }
    
 


    
    
}