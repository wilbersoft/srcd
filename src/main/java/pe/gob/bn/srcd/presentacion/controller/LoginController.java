package pe.gob.bn.srcd.presentacion.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes
public class LoginController {
 
	@RequestMapping("/login")
	public String login(Model m) {
		String message = "Hello World, Spring MVC @ Javatpoint";
		m.addAttribute("message", message);
		return "usuario/home"; 
	}
	
	
}
