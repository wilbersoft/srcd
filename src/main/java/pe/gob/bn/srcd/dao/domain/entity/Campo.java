package pe.gob.bn.srcd.dao.domain.entity;

public class Campo {
	
	private String f05IdCampo;
	private String f05CodOrden;
	private String f05Etiqueta;
	private String f05Nombre;
	private String f05CodTipoDato;
	private String f05Obligatorio;
	private String f05FechaReg;
	private String f05IdPlantilla;
	private String f05CodEstado;
	private String f05CodUsrReg;
	
	
	
	public Campo(String f05IdCampo, String f05CodOrden, String f05Etiqueta, String f05Nombre, String f05CodTipoDato,
			String f05Obligatorio, String f05FechaReg, String f05IdPlantilla, String f05CodEstado,
			String f05CodUsrReg) {
		super();
		this.f05IdCampo = f05IdCampo;
		this.f05CodOrden = f05CodOrden;
		this.f05Etiqueta = f05Etiqueta;
		this.f05Nombre = f05Nombre;
		this.f05CodTipoDato = f05CodTipoDato;
		this.f05Obligatorio = f05Obligatorio;
		this.f05FechaReg = f05FechaReg;
		this.f05IdPlantilla = f05IdPlantilla;
		this.f05CodEstado = f05CodEstado;
		this.f05CodUsrReg = f05CodUsrReg;
	}
	public String getF05IdCampo() {
		return f05IdCampo;
	}
	public void setF05IdCampo(String f05IdCampo) {
		this.f05IdCampo = f05IdCampo;
	}
	public String getF05CodOrden() {
		return f05CodOrden;
	}
	public void setF05CodOrden(String f05CodOrden) {
		this.f05CodOrden = f05CodOrden;
	}
	public String getF05Etiqueta() {
		return f05Etiqueta;
	}
	public void setF05Etiqueta(String f05Etiqueta) {
		this.f05Etiqueta = f05Etiqueta;
	}
	public String getF05Nombre() {
		return f05Nombre;
	}
	public void setF05Nombre(String f05Nombre) {
		this.f05Nombre = f05Nombre;
	}
	public String getF05CodTipoDato() {
		return f05CodTipoDato;
	}
	public void setF05CodTipoDato(String f05CodTipoDato) {
		this.f05CodTipoDato = f05CodTipoDato;
	}
	public String getF05Obligatorio() {
		return f05Obligatorio;
	}
	public void setF05Obligatorio(String f05Obligatorio) {
		this.f05Obligatorio = f05Obligatorio;
	}
	public String getF05FechaReg() {
		return f05FechaReg;
	}
	public void setF05FechaReg(String f05FechaReg) {
		this.f05FechaReg = f05FechaReg;
	}
	public String getF05IdPlantilla() {
		return f05IdPlantilla;
	}
	public void setF05IdPlantilla(String f05IdPlantilla) {
		this.f05IdPlantilla = f05IdPlantilla;
	}
	public String getF05CodEstado() {
		return f05CodEstado;
	}
	public void setF05CodEstado(String f05CodEstado) {
		this.f05CodEstado = f05CodEstado;
	}
	public String getF05CodUsrReg() {
		return f05CodUsrReg;
	}
	public void setF05CodUsrReg(String f05CodUsrReg) {
		this.f05CodUsrReg = f05CodUsrReg;
	}
	

	
	
}
