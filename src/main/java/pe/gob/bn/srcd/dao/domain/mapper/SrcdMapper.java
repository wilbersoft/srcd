package pe.gob.bn.srcd.dao.domain.mapper;

import java.util.List;

import pe.gob.bn.srcd.dao.domain.entity.Anexo;
import pe.gob.bn.srcd.dao.domain.entity.Plantilla;


public interface  SrcdMapper {
	
	public List<Anexo> selectAnexo(Anexo appAnexo);
	public List<Anexo> selectAnexoById(Anexo appAnexo);
	public List<Anexo> selectCtaContable(Anexo appAnexo);
	public int  insertPlantilla(Plantilla plantilla);
	public int  updateAnexo(Anexo anexo);
	public Integer  getIdPlantilla();
	  
}
