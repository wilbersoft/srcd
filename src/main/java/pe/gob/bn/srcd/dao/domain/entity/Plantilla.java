package pe.gob.bn.srcd.dao.domain.entity;

public class Plantilla {

	private String f04IdPlantilla;
	private String f04CuentaContable;
	private String f04Nombre;
	private String f04CodEstado;
	private String f04FechaReg;
	private String f04CodUsrReg;
	
 
	
	public Plantilla(String f04IdPlantilla, String f04CuentaContable, String f04Nombre, String f04CodEstado,
			String f04FechaReg, String f04CodUsrReg) {
		super();
		this.f04IdPlantilla = f04IdPlantilla;
		this.f04CuentaContable = f04CuentaContable;
		this.f04Nombre = f04Nombre;
		this.f04CodEstado = f04CodEstado;
		this.f04FechaReg = f04FechaReg;
		this.f04CodUsrReg = f04CodUsrReg;
	}
	
	public String getF04IdPlantilla() {
		return f04IdPlantilla;
	}
	public void setF04IdPlantilla(String f04IdPlantilla) {
		this.f04IdPlantilla = f04IdPlantilla;
	}
	public String getF04CuentaContable() {
		return f04CuentaContable;
	}
	public void setF04CuentaContable(String f04CuentaContable) {
		this.f04CuentaContable = f04CuentaContable;
	}
	public String getF04Nombre() {
		return f04Nombre;
	}
	public void setF04Nombre(String f04Nombre) {
		this.f04Nombre = f04Nombre;
	}
	public String getF04CodEstado() {
		return f04CodEstado;
	}
	public void setF04CodEstado(String f04CodEstado) {
		this.f04CodEstado = f04CodEstado;
	}
	public String getF04FechaReg() {
		return f04FechaReg;
	}
	public void setF04FechaReg(String f04FechaReg) {
		this.f04FechaReg = f04FechaReg;
	}
	public String getF04CodUsrReg() {
		return f04CodUsrReg;
	}
	public void setF04CodUsrReg(String f04CodUsrReg) {
		this.f04CodUsrReg = f04CodUsrReg;
	}
	
	
}
