package pe.gob.bn.srcd.dao.domain.entity;

public class Anexo {

	private String f02IdAnexo;
	private String f02IdPlantilla;
	private String f02FechProc;
	private String f02FechaCorte;
	private String f02CuentaContable;
	private String f02CodigoCliente;
	private String f02NombreCliente;
	private String f02Moneda;
	private Double f02Importe;
	private String f02CEtiqueta1;
	private String f02CValor1;
	private Integer f02CTipoDato1;
	private Integer f02CObligatorio1;
	private String f02CEtiqueta2;
	private String f02CValor2;
	private Integer f02CTipoDato2;
	private Integer f02CObligatorio2;
	private String f02CEtiqueta3;
	private String f02CValor3;
	private Integer f02CTipoDato3;
	private Integer f02CObligatorio3;
	
	private String f02CEtiqueta4;
	private String f02CValor4;
	private Integer f02CTipoDato4;
	private Integer f02CObligatorio4;
	
	private String f02CEtiqueta5;
	private String f02CValor5;
	private Integer f02CTipoDato5;
	private Integer f02CObligatorio5;
	
	private String f02CEtiqueta6;
	private String f02CValor6;
	private Integer f02CTipoDato6;
	private Integer f02CObligatorio6;
	
	private String f02CEtiqueta7;
	private String f02CValor7;
	private Integer f02CTipoDato7;
	private Integer f02CObligatorio7;
	
	private String f02CEtiqueta8;
	private String f02CValor8;
	private Integer f02CTipoDato8;
	private Integer f02CObligatorio8;
	
	private String f02CEtiqueta9;
	private String f02CValor9;
	private Integer f02CTipoDato9;
	private Integer f02CObligatorio9;
	
	private String f02CEtiqueta10;
	private String f02CValor10;
	private Integer f02CTipoDato10;
	private Integer f02CObligatorio10;
	
	
	
	private String f02FechaReg;
	private String f02CodUsrReg;
	
	
	public Anexo() {
		
	}
	
	public Anexo(String f02FechaCorte, String f02CuentaContable, String f02CodigoCliente) {
		super();
		this.f02FechaCorte = f02FechaCorte;
		this.f02CuentaContable = f02CuentaContable;
		this.f02CodigoCliente = f02CodigoCliente;
	}
	
	
 

	public Anexo(
			String f02FechaCorte, 
			String f02CuentaContable, 
			String f02CodigoCliente, 
			Double f02Importe,
			String f02cValor1, 
			String f02cValor2, 
			String f02cValor3,  
			String f02cValor4,
			String f02cValor5
			) {
		super();
		this.f02FechaCorte = f02FechaCorte;
		this.f02CuentaContable = f02CuentaContable;
		this.f02CodigoCliente = f02CodigoCliente;
		this.f02Importe = f02Importe;
		f02CValor1 = f02cValor1;
		f02CValor2 = f02cValor2;
		f02CValor3 = f02cValor3;
		f02CValor4 = f02cValor4;
		f02CValor5 = f02cValor5;
	}

	public String getF02IdAnexo() {
		return f02IdAnexo;
	}
	public void setF02IdAnexo(String f02IdAnexo) {
		this.f02IdAnexo = f02IdAnexo;
	}
	public String getF02IdPlantilla() {
		return f02IdPlantilla;
	}
	public void setF02IdPlantilla(String f02IdPlantilla) {
		this.f02IdPlantilla = f02IdPlantilla;
	}
	public String getF02FechProc() {
		return f02FechProc;
	}
	public void setF02FechProc(String f02FechProc) {
		this.f02FechProc = f02FechProc;
	}
	public String getF02FechaCorte() {
		return f02FechaCorte;
	}
	public void setF02FechaCorte(String f02FechaCorte) {
		this.f02FechaCorte = f02FechaCorte;
	}
	public String getF02CuentaContable() {
		return f02CuentaContable;
	}
	public void setF02CuentaContable(String f02CuentaContable) {
		this.f02CuentaContable = f02CuentaContable;
	}
	public String getF02CodigoCliente() {
		return f02CodigoCliente;
	}
	public void setF02CodigoCliente(String f02CodigoCliente) {
		this.f02CodigoCliente = f02CodigoCliente;
	}
	public String getF02NombreCliente() {
		return f02NombreCliente;
	}
	public void setF02NombreCliente(String f02NombreCliente) {
		this.f02NombreCliente = f02NombreCliente;
	}
	public String getF02Moneda() {
		return f02Moneda;
	}
	public void setF02Moneda(String f02Moneda) {
		this.f02Moneda = f02Moneda;
	}
	public Double getF02Importe() {
		return f02Importe;
	}
	public void setF02Importe(Double f02Importe) {
		this.f02Importe = f02Importe;
	}
	public String getF02CEtiqueta1() {
		return f02CEtiqueta1;
	}
	public void setF02CEtiqueta1(String f02cEtiqueta1) {
		f02CEtiqueta1 = f02cEtiqueta1;
	}
	public String getF02CValor1() {
		return f02CValor1;
	}
	public void setF02CValor1(String f02cValor1) {
		f02CValor1 = f02cValor1;
	}
	public Integer getF02CTipoDato1() {
		return f02CTipoDato1;
	}
	public void setF02CTipoDato1(Integer f02cTipoDato1) {
		f02CTipoDato1 = f02cTipoDato1;
	}
	public Integer getF02CObligatorio1() {
		return f02CObligatorio1;
	}
	public void setF02CObligatorio1(Integer f02cObligatorio1) {
		f02CObligatorio1 = f02cObligatorio1;
	}
	public String getF02CEtiqueta2() {
		return f02CEtiqueta2;
	}
	public void setF02CEtiqueta2(String f02cEtiqueta2) {
		f02CEtiqueta2 = f02cEtiqueta2;
	}
	public String getF02CValor2() {
		return f02CValor2;
	}
	public void setF02CValor2(String f02cValor2) {
		f02CValor2 = f02cValor2;
	}
	public Integer getF02CTipoDato2() {
		return f02CTipoDato2;
	}
	public void setF02CTipoDato2(Integer f02cTipoDato2) {
		f02CTipoDato2 = f02cTipoDato2;
	}
	public Integer getF02CObligatorio2() {
		return f02CObligatorio2;
	}
	public void setF02CObligatorio2(Integer f02cObligatorio2) {
		f02CObligatorio2 = f02cObligatorio2;
	}
	public String getF02CEtiqueta3() {
		return f02CEtiqueta3;
	}
	public void setF02CEtiqueta3(String f02cEtiqueta3) {
		f02CEtiqueta3 = f02cEtiqueta3;
	}
	public String getF02CValor3() {
		return f02CValor3;
	}
	public void setF02CValor3(String f02cValor3) {
		f02CValor3 = f02cValor3;
	}
	public Integer getF02CTipoDato3() {
		return f02CTipoDato3;
	}
	public void setF02CTipoDato3(Integer f02cTipoDato3) {
		f02CTipoDato3 = f02cTipoDato3;
	}
	public Integer getF02CObligatorio3() {
		return f02CObligatorio3;
	}
	public void setF02CObligatorio3(Integer f02cObligatorio3) {
		f02CObligatorio3 = f02cObligatorio3;
	}
	public String getF02CEtiqueta4() {
		return f02CEtiqueta4;
	}
	public void setF02CEtiqueta4(String f02cEtiqueta4) {
		f02CEtiqueta4 = f02cEtiqueta4;
	}
	public String getF02CValor4() {
		return f02CValor4;
	}
	public void setF02CValor4(String f02cValor4) {
		f02CValor4 = f02cValor4;
	}
	public Integer getF02CTipoDato4() {
		return f02CTipoDato4;
	}
	public void setF02CTipoDato4(Integer f02cTipoDato4) {
		f02CTipoDato4 = f02cTipoDato4;
	}
	public Integer getF02CObligatorio4() {
		return f02CObligatorio4;
	}
	public void setF02CObligatorio4(Integer f02cObligatorio4) {
		f02CObligatorio4 = f02cObligatorio4;
	}
	public String getF02FechaReg() {
		return f02FechaReg;
	}
	public void setF02FechaReg(String f02FechaReg) {
		this.f02FechaReg = f02FechaReg;
	}
	public String getF02CodUsrReg() {
		return f02CodUsrReg;
	}
	public void setF02CodUsrReg(String f02CodUsrReg) {
		this.f02CodUsrReg = f02CodUsrReg;
	}

	public String getF02CEtiqueta5() {
		return f02CEtiqueta5;
	}

	public void setF02CEtiqueta5(String f02cEtiqueta5) {
		f02CEtiqueta5 = f02cEtiqueta5;
	}

	public String getF02CValor5() {
		return f02CValor5;
	}

	public void setF02CValor5(String f02cValor5) {
		f02CValor5 = f02cValor5;
	}

	public Integer getF02CTipoDato5() {
		return f02CTipoDato5;
	}

	public void setF02CTipoDato5(Integer f02cTipoDato5) {
		f02CTipoDato5 = f02cTipoDato5;
	}

	public Integer getF02CObligatorio5() {
		return f02CObligatorio5;
	}

	public void setF02CObligatorio5(Integer f02cObligatorio5) {
		f02CObligatorio5 = f02cObligatorio5;
	}

	public String getF02CEtiqueta6() {
		return f02CEtiqueta6;
	}

	public void setF02CEtiqueta6(String f02cEtiqueta6) {
		f02CEtiqueta6 = f02cEtiqueta6;
	}

	public String getF02CValor6() {
		return f02CValor6;
	}

	public void setF02CValor6(String f02cValor6) {
		f02CValor6 = f02cValor6;
	}

	public Integer getF02CTipoDato6() {
		return f02CTipoDato6;
	}

	public void setF02CTipoDato6(Integer f02cTipoDato6) {
		f02CTipoDato6 = f02cTipoDato6;
	}

	public Integer getF02CObligatorio6() {
		return f02CObligatorio6;
	}

	public void setF02CObligatorio6(Integer f02cObligatorio6) {
		f02CObligatorio6 = f02cObligatorio6;
	}

	public String getF02CEtiqueta7() {
		return f02CEtiqueta7;
	}

	public void setF02CEtiqueta7(String f02cEtiqueta7) {
		f02CEtiqueta7 = f02cEtiqueta7;
	}

	public String getF02CValor7() {
		return f02CValor7;
	}

	public void setF02CValor7(String f02cValor7) {
		f02CValor7 = f02cValor7;
	}

	public Integer getF02CTipoDato7() {
		return f02CTipoDato7;
	}

	public void setF02CTipoDato7(Integer f02cTipoDato7) {
		f02CTipoDato7 = f02cTipoDato7;
	}

	public Integer getF02CObligatorio7() {
		return f02CObligatorio7;
	}

	public void setF02CObligatorio7(Integer f02cObligatorio7) {
		f02CObligatorio7 = f02cObligatorio7;
	}

	public String getF02CEtiqueta8() {
		return f02CEtiqueta8;
	}

	public void setF02CEtiqueta8(String f02cEtiqueta8) {
		f02CEtiqueta8 = f02cEtiqueta8;
	}

	public String getF02CValor8() {
		return f02CValor8;
	}

	public void setF02CValor8(String f02cValor8) {
		f02CValor8 = f02cValor8;
	}

	public Integer getF02CTipoDato8() {
		return f02CTipoDato8;
	}

	public void setF02CTipoDato8(Integer f02cTipoDato8) {
		f02CTipoDato8 = f02cTipoDato8;
	}

	public Integer getF02CObligatorio8() {
		return f02CObligatorio8;
	}

	public void setF02CObligatorio8(Integer f02cObligatorio8) {
		f02CObligatorio8 = f02cObligatorio8;
	}

	public String getF02CEtiqueta9() {
		return f02CEtiqueta9;
	}

	public void setF02CEtiqueta9(String f02cEtiqueta9) {
		f02CEtiqueta9 = f02cEtiqueta9;
	}

	public String getF02CValor9() {
		return f02CValor9;
	}

	public void setF02CValor9(String f02cValor9) {
		f02CValor9 = f02cValor9;
	}

	public Integer getF02CTipoDato9() {
		return f02CTipoDato9;
	}

	public void setF02CTipoDato9(Integer f02cTipoDato9) {
		f02CTipoDato9 = f02cTipoDato9;
	}

	public Integer getF02CObligatorio9() {
		return f02CObligatorio9;
	}

	public void setF02CObligatorio9(Integer f02cObligatorio9) {
		f02CObligatorio9 = f02cObligatorio9;
	}

	public String getF02CEtiqueta10() {
		return f02CEtiqueta10;
	}

	public void setF02CEtiqueta10(String f02cEtiqueta10) {
		f02CEtiqueta10 = f02cEtiqueta10;
	}

	public String getF02CValor10() {
		return f02CValor10;
	}

	public void setF02CValor10(String f02cValor10) {
		f02CValor10 = f02cValor10;
	}

	public Integer getF02CTipoDato10() {
		return f02CTipoDato10;
	}

	public void setF02CTipoDato10(Integer f02cTipoDato10) {
		f02CTipoDato10 = f02cTipoDato10;
	}

	public Integer getF02CObligatorio10() {
		return f02CObligatorio10;
	}

	public void setF02CObligatorio10(Integer f02cObligatorio10) {
		f02CObligatorio10 = f02cObligatorio10;
	}
	
	
	
	
	
	
}
