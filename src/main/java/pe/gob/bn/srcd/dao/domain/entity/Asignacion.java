package pe.gob.bn.srcd.dao.domain.entity;

public class Asignacion {

	private String f03IdAsignacion;
	private String f03Usuario;
	private String f03CuentaContable;
	private String f03FechProc;
	private String f03CodigoEmpleado;
	private String f03Dni;
	private String f03Nombres;
	private String f03ApePaterno;
	private String f03ApeMaterno;
	private String f03CodArea;
	private String f03Area;
	private String f03CodEstado;
	private String f03UsrReg;
	private String f03FechaReg;
	
	
	
	public Asignacion(String f03IdAsignacion, String f03Usuario, String f03CuentaContable, String f03FechProc,
			String f03CodigoEmpleado, String f03Dni, String f03Nombres, String f03ApePaterno, String f03ApeMaterno,
			String f03CodArea, String f03Area, String f03CodEstado, String f03UsrReg, String f03FechaReg) {
		super();
		this.f03IdAsignacion = f03IdAsignacion;
		this.f03Usuario = f03Usuario;
		this.f03CuentaContable = f03CuentaContable;
		this.f03FechProc = f03FechProc;
		this.f03CodigoEmpleado = f03CodigoEmpleado;
		this.f03Dni = f03Dni;
		this.f03Nombres = f03Nombres;
		this.f03ApePaterno = f03ApePaterno;
		this.f03ApeMaterno = f03ApeMaterno;
		this.f03CodArea = f03CodArea;
		this.f03Area = f03Area;
		this.f03CodEstado = f03CodEstado;
		this.f03UsrReg = f03UsrReg;
		this.f03FechaReg = f03FechaReg;
	}
	
	public String getF03IdAsignacion() {
		return f03IdAsignacion;
	}
	public void setF03IdAsignacion(String f03IdAsignacion) {
		this.f03IdAsignacion = f03IdAsignacion;
	}
	public String getF03Usuario() {
		return f03Usuario;
	}
	public void setF03Usuario(String f03Usuario) {
		this.f03Usuario = f03Usuario;
	}
	public String getF03CuentaContable() {
		return f03CuentaContable;
	}
	public void setF03CuentaContable(String f03CuentaContable) {
		this.f03CuentaContable = f03CuentaContable;
	}
	public String getF03FechProc() {
		return f03FechProc;
	}
	public void setF03FechProc(String f03FechProc) {
		this.f03FechProc = f03FechProc;
	}
	public String getF03CodigoEmpleado() {
		return f03CodigoEmpleado;
	}
	public void setF03CodigoEmpleado(String f03CodigoEmpleado) {
		this.f03CodigoEmpleado = f03CodigoEmpleado;
	}
	public String getF03Dni() {
		return f03Dni;
	}
	public void setF03Dni(String f03Dni) {
		this.f03Dni = f03Dni;
	}
	public String getF03Nombres() {
		return f03Nombres;
	}
	public void setF03Nombres(String f03Nombres) {
		this.f03Nombres = f03Nombres;
	}
	public String getF03ApePaterno() {
		return f03ApePaterno;
	}
	public void setF03ApePaterno(String f03ApePaterno) {
		this.f03ApePaterno = f03ApePaterno;
	}
	public String getF03ApeMaterno() {
		return f03ApeMaterno;
	}
	public void setF03ApeMaterno(String f03ApeMaterno) {
		this.f03ApeMaterno = f03ApeMaterno;
	}
	public String getF03CodArea() {
		return f03CodArea;
	}
	public void setF03CodArea(String f03CodArea) {
		this.f03CodArea = f03CodArea;
	}
	public String getF03Area() {
		return f03Area;
	}
	public void setF03Area(String f03Area) {
		this.f03Area = f03Area;
	}
	public String getF03CodEstado() {
		return f03CodEstado;
	}
	public void setF03CodEstado(String f03CodEstado) {
		this.f03CodEstado = f03CodEstado;
	}
	public String getF03UsrReg() {
		return f03UsrReg;
	}
	public void setF03UsrReg(String f03UsrReg) {
		this.f03UsrReg = f03UsrReg;
	}
	public String getF03FechaReg() {
		return f03FechaReg;
	}
	public void setF03FechaReg(String f03FechaReg) {
		this.f03FechaReg = f03FechaReg;
	}
	
	
	
	
}
