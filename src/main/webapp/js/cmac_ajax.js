function validaSoloNumerico(cadena){

    var patron = /^\d*$/;

    if(!cadena.search(patron))

      return true;

    else{
    	return false;
    }

      
 }

function validaDni(cadena){

	if(cadena==null){
		alert('Ingrese el DNI');
	}
	var n = cadena.trim();
    if(n.length==8){
    	if(validaSoloNumerico(n)){
    		return true;
    	}else{
        	alert('El DNI debe ser un n�mero');
        	return false;
    	};
    }
    else{
    	alert('El DNI debe tener 8 d�gitos');
    	return false;	
    }
      
 }

function disableEnterKey(e){ 
 
var key; 
    if(window.event){ 
    key = window.event.keyCode; 
    } else { 
    key = e.which;      
    } 
    if(key == 13){ 
    return false; 
    } else { 
    return true; 
    } 
}

function stopRKey(evt) { 
	  var evt = (evt) ? evt : ((event) ? event : null); 
	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
	  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
} 

document.onkeypress = stopRKey;
	


function ImprimirDoc(pantalla){
 
	//Ocultar todos los objetos
	var objeto = document.getElementsByTagName('TABLE');
	for(i = 0; i < objeto.length; i++){
       objeto[i].style.visibility = 'hidden';
   }
	 
   pantalla.style.position='absolute';
   pantalla.style.visibility = 'visible';
   pantalla.style.top = '0px' ;
   pantalla.style.left = '0px' ;
   
   var hijos = pantalla.getElementsByTagName('TABLE');
	for(i = 0; i < hijos.length; i++){
       hijos[i].style.visibility = 'visible';
   }
	 
  // tabHeaderImprimir.style.display = '';
//   tabFooterImprimir.style.display = '';
 window.print();
   
   //Volver al estado Normal
	var objeto = document.getElementsByTagName('TABLE');
	for(i = 0; i < objeto.length; i++){
       objeto[i].style.visibility = 'visible';
   }
   pantalla.style.position='static';
   //tabFooterImprimir.style.display = 'none';
   //tabHeaderImprimir.style.display = 'none';
} 


function nuevoAjaxx()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false;
	try 
	{
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			// Creacion del objet AJAX para IE
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E)
		{
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') xmlhttp=new XMLHttpRequest();
		}
	}
	return xmlhttp; 
}  

/******							******/
//C�digo JavaScript:   
function FAjax(url,capa,valores)
{   	  
		 
          var ajax=nuevoAjaxx();
          var capaContenedora = document.getElementById(capa);
          capaContenedora.innerHTML="<center> <img  src='img/loading.gif'></center>";
          ajax.open ('POST', url, true);
          ajax.onreadystatechange = function() {
          if (ajax.readyState==1) {
            capaContenedora.innerHTML="<center> <img  src='img/loading.gif'></center>";
          }
          else if (ajax.readyState==4){
                   if(ajax.status==200)
                   {
                        document.getElementById(capa).innerHTML=ajax.responseText;
                   }
                   else if(ajax.status==404)
                   {
                	   capaContenedora.innerHTML = ""+"<div>Error:" + ajax.status +  "</div>";
                   }
                   }
            }

          ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1');
         ajax.send(valores);
         return;
} 


//Validacion de los radioBttns
function ValidarRadio(optSeleccion){
	
	 
	//obteniendo el objeto radioBttns.
	var radioBttn = document.getElementsByName(optSeleccion);
 
	if (radioBttn == null){
		alert("No existe elementos en la grilla");
		return false;
	}

	var cadena = "" + radioBttn.length;
	if (cadena == "undefined"){
	   
		//existe un solo elemento
		if (radioBttn.checked==true){
			return true;
		}else{	
		alert("Seleccione una sola opcion");		
		return false;}
	}
	else{
		//Esta en una coleccion
		if (radioBttn.length == 0){
			alert("No existe elementos en la grilla");
			return false;}
		var icont = 0;	

		//verificando que por lo menos exiete un radioBttn habilitado
		for (i=0;i<radioBttn.length;i++)
			if (radioBttn[i].checked==true) 
					icont++;
		if(icont == 1){
			return true;
		}

		if(icont == 0)
			alert("Seleccione un elemento de la grilla");
		else
			alert("Seleccione una sola opcion");
	} 
	return false;
}

function CAjax(url,valores)
{   	  
		 
          var ajax=nuevoAjaxx();
          ajax.open ('POST', url, true);

          ajax.onreadystatechange = function() {
        	  
        	  if (ajax.readyState==4 && ajax.status==200){
        		  
        			 //Pintando Valores
        		     var valores = ajax.responseText;
        		     
        			 if(valores==null || valores.length==0){
        				 alert('No existe DNI en la lista de usuarios');
        			    return;
        			 }
        			 
        		     document.getElementById('nombres').value=='';
        		     document.getElementById('apePaterno').value=='';
        		     document.getElementById('apeMaterno').value=='';
        			 
        		     var valoresPipe= valores.split("|");
        			 
        		    if(valoresPipe!=null && is_array(valoresPipe) && valoresPipe.length==3){

        		        document.getElementById('apePaterno').value=valoresPipe[0];
        		        document.getElementById('apeMaterno').value=valoresPipe[1];
        		        document.getElementById('nombres').value=valoresPipe[2];

        		    }else{
        		     alert('Los datos del usuario no son validos');
        		     return;
        		    };
        		  return;
        	  }else{
        		 
        	  }
          }
          
          ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1');
         ajax.send(valores);
         return;
} 

