<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
 <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<% 
//response.setHeader("Cache-Control","no-store"); //HTTP 1.1
//response.setHeader("Pragma","no-cache"); //HTTP 1.0
//response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/prototype.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/scriptaculous.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/ajaxtags-1.2-beta2.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>

<link href="https://getbootstrap.com/docs/4.1/dist/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"></script>



<script>
$.noConflict();
// Code that uses other library's $ can follow here.
</script>
  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cmac Huancayo</title>

<link rel="stylesheet" href="css/cmac.css" type="text/css" />
<link rel="stylesheet" href="css/cmac1.css" type="text/css" />
<link rel="stylesheet" href="css/cmac2.css" type="text/css" />
<link rel="stylesheet" href="css/cmac3.css" type="text/css" />
<link rel="stylesheet" href="css/styles.css" type="text/css" />

<SCRIPT language=javascript>

	function endSesion(){
		
		document.frmAction.action="login.html";
		document.frmAction.method.value="caducaSesion";
		document.frmAction.submit();
	}

</SCRIPT>

</head>

<BODY    bgColor=#e5e5dd  leftMargin=0 topMargin=0 MS_POSITIONING="FlowLayout" >

<form  id="frmAction" name="frmAction" method="post" action="" runat="server" >
<input type="hidden" name="method" id="method" value="">
</form>

<div class="rxsviewport">	
	<table class="rxsviewport" cellpadding="0" cellspacing="0">
		<tr> 
			<td height="80px" valign="top">			
				<table class="rxcmacica" cellpadding="4" cellspacing="1" height="80px">

					<tr>
						<td  valign="top" height="30px" style="background-color: white;">
								<tiles:insertAttribute name="header" />
						</td>						
						</tr>
				
					<tr>
						<td class="rxcontainer" valign="top" height="30px" style="background-color: white;">
							<div class="cysMenubar">
								
							</div> 
							<div class="cysSessionbar">
								<span class="cysUsername"><c:out value="${usuario.cuenta}" /></span>&nbsp;&nbsp;
								<span><c:out value="${usuario.nombre}" />&nbsp;
                                      <c:out value="${usuario.apePaterno}" />&nbsp;
                                      <c:out value="${usuario.apeMaterno}" /></span> 
								<span><a href="javascript:;" onclick="endSesion();">Cerrar Sesi&oacute;n</a></span>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<tiles:insertAttribute name="menu" />			
			</td>
			<td>			
				<div class="rxverticalview">
				<!-- vertical view -->
			
				<table border="0" class="rxsview" align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td width="10px" height="12px"><img src="css/rxs/topleftback.gif" /></td>
						<td class="rxborderwhite">&nbsp;</td>
						<td width="10px" height="12px"><img src="css/rxs/toprightback.gif" /></td>
					</tr>
					<tr>
						<td class="rxborderwhite">&nbsp;</td>
						<td class="rxborderwhite">
						<!--  Interfase -->
					
						<table class="rxsviewport">
							<tr>
								<td valign="top">
									 <!-- App container -->
								
								<tiles:insertAttribute name="body" />
									
									<!-- End App container -->
											
								</td>
							</tr>
						</table>
				
				
						<!-- End Interfase -->
						</td>
						<td class="rxborderright">&nbsp;</td>
					</tr>
					<tr>
						<td width="10px" valign="top"><img src="css/rxs/bottomleftback.gif" /></td>
						<td class="rxborderbottom">&nbsp;</td>
						<td width="10px" valign="top"><img src="css/rxs/bottomrightback.gif" /></td>
					</tr>
				</table>
	
				<!-- End vertical view -->
				</div>
			</td>
		</tr>
	</table>
	


	
</div>

<div id="rxsLoading" class="rxfloat" style="display:none; z-index: 210">

 
	
</div>

</body>
</html>