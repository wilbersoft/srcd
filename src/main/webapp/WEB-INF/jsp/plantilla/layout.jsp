<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
 <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/cmac.css" type="text/css" />
<link rel="stylesheet" href="css/cmac1.css" type="text/css" />
<link rel="stylesheet" href="css/cmac2.css" type="text/css" />
<link rel="stylesheet" href="css/cmac3.css" type="text/css" />
<link rel="stylesheet" href="css/styles.css" type="text/css" />
  <!-- 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   -->
      
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
    
</head>
<body>
<div class="header">
  
 <tiles:insertAttribute name="header" />
</div>
<nav class="navbar navbar-inverse" style="height: 20px;"  >
  <div class="container-fluid">
   
    <ul class="nav navbar-nav">
     
      
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Plantilla <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Registrar Plantilla</a></li>
          <li><a href="#">Consultar Plantilla</a></li>
          
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Asignación <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Registrar Asignación</a></li>
          <li><a href="#">Consultar Asignación</a></li>
        </ul>
      </li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Anexo <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="descargarAnexo.html" >Descargar Anexo</a></li>
          <li><a href="subirAnexo.html"  >Subir Anexo</a></li>
        </ul>
      </li>      
      
      
    	
    </ul>
    
 <ul class="nav navbar-nav navbar-right">
      
      <li><a href="index.jsp"><span class="glyphicon glyphicon-log-in"></span>Cerrar Sesion</a></li>
    </ul>
        
  </div>
</nav>


  
<div class="rxverticalview">

				<div >
				<!-- vertical view -->
			
				<table border="0" class="rxsview" align="center" cellpadding="0" cellspacing="0">
				 
					<tr>
						<td class="rxborderwhite">&nbsp;</td>
						<td class="rxborderwhite">
						<!--  Interfase -->
					
						<table class="rxsviewport">
							<tr>
								<td valign="top">
									 <!-- App container -->
								
								<tiles:insertAttribute name="body" />
									
									<!-- End App container -->
											
								</td>
							</tr>
						</table>
				
				
						<!-- End Interfase -->
						</td>
						<td class="rxborderright">&nbsp;</td>
					</tr>
			 
				</table>
	
				<!-- End vertical view -->
				</div>
 
</div>

</body>
</html>
 